//
//  MockFlowController.swift
//  NYCDemoAppTests
//
//  Created by Ashish Singh on 3/15/23.
//

import Foundation
@testable import NYCDemoApp

class MockFlowController: FlowControllable {
    var destination: Destination!
    
    func handleNavigation(destination: Destination) {
        self.destination = destination
    }
}
