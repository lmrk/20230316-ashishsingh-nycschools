//
//  NetworkManager.swift
//  NYCDemoApp
//
//  Created by Ashish Singh on 3/14/23.
//

import Foundation
protocol NetworkHandler {
    func fetchSchools(completion: @escaping (Result<[SchoolDataModel], AppErrors>) -> Void)
    func fetchDetail(id: String, completion: @escaping (Result<[SchoolDetailDataModel], AppErrors>) -> Void)
}

enum AppErrors: Error {
    case network
    case partial
}

class NetworkManager: NetworkHandler {
    enum BaseUrl: String {
        case schools = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        case schoolDetail = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    }

    // Instead of fetching complete list, API should support to return data in batches
    // client can pass the index if data it needs in next batch
    func fetchSchools(completion: @escaping (Result<[SchoolDataModel], AppErrors>) -> Void) {
        guard let url = URL(string: BaseUrl.schools.rawValue) else {return}
        let request = URLRequest(url: url, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 10)
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil  else {
                return completion(.failure(.network))
            }
            
            do {
                let data = try JSONDecoder().decode([SchoolDataModel].self, from: data)
                completion(.success(data))
            } catch {
                completion(.failure(.network))
            }
        }.resume()
    }
    
    func fetchDetail(id: String, completion: @escaping (Result<[SchoolDetailDataModel], AppErrors>) -> Void) {
        guard let url = URL(string: BaseUrl.schoolDetail.rawValue) else {return}
        let request = URLRequest(url: url, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 10)
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil  else {
                return completion(.failure(.network))
            }
            
            do {
                let data = try JSONDecoder().decode([SchoolDetailDataModel].self, from: data)
                completion(.success(data))
            } catch {
                completion(.failure(.network))
            }
        }.resume()
    }

}
