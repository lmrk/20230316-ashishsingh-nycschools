//
//  SchoolDataModel.swift
//  NYCDemoApp
//
//  Created by Ashish Singh on 3/14/23.
//

import Foundation

struct SchoolDataModel: Decodable {
    var dbn: String?
    var school_name: String?
    var overview_paragraph: String?
    var campus_name: String?
}

extension SchoolDataModel: Equatable {
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.school_name == rhs.school_name
        && lhs.overview_paragraph == rhs.overview_paragraph
        && lhs.campus_name == rhs.campus_name
    }
}
