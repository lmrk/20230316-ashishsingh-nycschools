//
//  SchoolsViewModel.swift
//  NYCDemoApp
//
//  Created by Ashish Singh on 3/14/23.
//

import Combine

enum Input {
    case fetchData
    case userSelection(dbnId: String)
}

protocol ViewModelActionable {
    var subject: PassthroughSubject<[SchoolDataModel], AppErrors> {get}
    func handleEvent(input: Input)
}

class SchoolsViewModel {
    private let flowController: FlowControllable
    private let networkManager: NetworkHandler
    var subject = PassthroughSubject<[SchoolDataModel], AppErrors>()
    
    // Data is saved in Set for faster retrival
    // Finding any detail dataModel would be done in constant time complexity
    var dataModelSet = Set<SchoolDetailDataModel>()
    
    // Dependency Injection
    // Would be very helpful in writing unit test and could be easily replaced by Mocl classes
    init(flowController: FlowControllable, networkManager: NetworkHandler) {
        self.flowController = flowController
        self.networkManager = networkManager
    }
}

extension SchoolsViewModel: ViewModelActionable {
    func handleEvent(input: Input) {
        switch input {
        case .fetchData:
            networkManager.fetchSchools { [weak self] result in
                switch result {
                case .success(let data):
                    self?.subject.send(data)
                case .failure(let error):
                    self?.subject.send(completion: .failure(error))
                }
            }
        case .userSelection(let dbnId):
            
            // if data available dont fetch from server again
            if let dataModel = getDataModel(for: dbnId) {
                flowController.handleNavigation(destination: .detailScreen(dataModel))
                return
            }

            networkManager.fetchDetail(id: dbnId) { [weak self] result in
                switch result {
                case .success(let data):
                    data.forEach { dataModel in
                        self?.dataModelSet.insert(dataModel)
                    }
                    if let dataModel = self?.getDataModel(for: dbnId) {
                        self?.flowController.handleNavigation(destination: .detailScreen(dataModel))
                    }
                case .failure(let error):
                    self?.subject.send(completion: .failure(error))
                }
            }
        }
    }
}

extension SchoolsViewModel {
    func getDataModel(for dbnId: String) -> SchoolDetailDataModel? {
        guard !dataModelSet.isEmpty else { return nil }
        
        let detailDataModel = dataModelSet.first {  $0.dbn == dbnId }
        guard let detailDataModel = detailDataModel else { return nil }
        return detailDataModel
    }
}

