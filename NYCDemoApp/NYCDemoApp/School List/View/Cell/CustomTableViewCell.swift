//
//  CustomTableViewCell.swift
//  NYCDemoApp
//
//  Created by Ashish Singh on 3/14/23.
//

import UIKit

import UIKit

class CustomTableViewCell: UITableViewCell {

    private var vStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .leading
        stack.distribution = .fill
        stack.spacing = 5
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    // Leverage builder method to get the label
    var titleLabel = UILabel.create(with: "", style: .normal())
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUp()
    }
    
    func setUp() {
        // add to vStack
        vStackView.addArrangedSubview(titleLabel)

        // add stack to content view
        contentView.addSubview(vStackView)
        
        setUpConstraints()
    }
    
    func setUpConstraints() {
        [vStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
         vStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10),
         vStackView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 10),
         vStackView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10)].forEach {
            $0.isActive = true
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
