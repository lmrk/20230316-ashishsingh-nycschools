//
//  SchoolDetailDataModel.swift
//  NYCDemoApp
//
//  Created by Ashish Singh on 3/14/23.
//

import Foundation

struct SchoolDetailDataModel: Decodable {
    var dbn: String?
    var school_name: String?
    var sat_critical_reading_avg_score: String?
    var sat_math_avg_score: String?
    var sat_writing_avg_score: String?
}

extension  SchoolDetailDataModel: Hashable {
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.dbn == rhs.dbn
    }
}
